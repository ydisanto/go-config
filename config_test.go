package config

import (
	"github.com/corbym/gocrest/is"
	"github.com/corbym/gocrest/then"
	"testing"
)

type TestConf struct {
	K1 string
}

func TestWorkingDirectoryConfigurationReadJson(t *testing.T) {

	configuration := FromWorkingDirectory().
		WithFileName("test/settings.json")

	testConf := &TestConf{}
	err := configuration.Read(testConf)
	if err != nil {
		t.Fatal("error loading configuration from file:", err)
	}

	then.AssertThat(t, testConf.K1, is.EqualTo("JL"))

}

func TestWorkingDirectoryConfigurationReadYaml(t *testing.T) {

	configuration := FromWorkingDirectory().
		WithFileName("test/settings.yml")

	testConf := &TestConf{}
	err := configuration.Read(testConf)
	if err != nil {
		t.Fatal("error loading configuration from file:", err)
	}

	then.AssertThat(t, testConf.K1, is.EqualTo("KO"))

}

func TestWorkingDirectoryConfigurationReadWithDefaultValue(t *testing.T) {
	defaultConf := &TestConf{K1: "v1"}

	configuration := FromWorkingDirectory().
		WithFileName("idontexist").
		WithDefaultValue(defaultConf)

	testConf := &TestConf{}
	err := configuration.Read(testConf)
	if err != nil {
		t.Fatal("error loading configuration from file:", err)
	}

	then.AssertThat(t, testConf.K1, is.EqualTo("v1"))
}
