package config

import (
	"errors"
	"io/fs"
	"os"
	"path/filepath"
)

func fileExists(filePath string) (bool, error) {
	_, err := os.Stat(filePath)
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			return false, nil
		} else {
			return false, err
		}
	}
	return true, nil
}

func executableDir() (string, error) {
	executablePath, err := os.Executable()
	if err != nil {
		return "", err
	}
	executablePath, err = filepath.EvalSymlinks(executablePath)
	if err != nil {
		return "", err
	}
	confDir := filepath.Dir(executablePath)
	return confDir, nil
}
