package config

import (
	"github.com/mitchellh/mapstructure"
	"gitlab.com/ydisanto/go-config/parsers"
	"os"
	"path"
	"path/filepath"
	"strings"
)

var DefaultConfigurationFileName = "settings.json"
var DefaultConfigurationFileFormat = ""

type Configuration struct {
	resolveFolder func() (string, error)
	FileName      string
	FileFormat    string
	DefaultValue  interface{}
}

func FromUserHome() *Configuration {
	return &Configuration{
		DefaultValue:  nil,
		FileName:      DefaultConfigurationFileName,
		resolveFolder: os.UserHomeDir,
		FileFormat:    DefaultConfigurationFileFormat,
	}
}

func FromWorkingDirectory() *Configuration {
	return &Configuration{
		DefaultValue:  nil,
		FileName:      DefaultConfigurationFileName,
		resolveFolder: os.Getwd,
		FileFormat:    DefaultConfigurationFileFormat,
	}
}

func FromExecutableDirectory() *Configuration {
	return &Configuration{
		DefaultValue:  nil,
		FileName:      DefaultConfigurationFileName,
		resolveFolder: executableDir,
		FileFormat:    DefaultConfigurationFileFormat,
	}
}

func (configuration *Configuration) WithFileName(fileName string) *Configuration {
	configuration.FileName = fileName
	return configuration
}

func (configuration *Configuration) WithFileFormat(format string) *Configuration {
	configuration.FileFormat = format
	return configuration
}

func (configuration *Configuration) WithDefaultValue(defaultValue interface{}) *Configuration {
	configuration.DefaultValue = defaultValue
	return configuration
}

func (configuration *Configuration) resolveFilePath() (string, error) {
	folder, err := configuration.resolveFolder()
	if err != nil {
		return "", err
	}
	return path.Join(folder, configuration.FileName), nil
}

func (configuration *Configuration) Read(output interface{}) error {
	filePath, err := configuration.resolveFilePath()
	if err != nil {
		return err
	}
	fileExists, err := fileExists(filePath)
	if err != nil {
		return err
	}
	if !fileExists {
		return mapstructure.Decode(configuration.DefaultValue, output)
	}

	parser := configuration.resolveParser(filePath)
	confValue, err := parser(filePath)
	if err != nil {
		return err
	}
	return mapstructure.Decode(confValue, output)
}

func (configuration *Configuration) resolveParser(filePath string) parsers.ConfigurationFileParser {
	format := strings.TrimSpace(configuration.FileFormat)
	if format == "" || format == "auto" {
		format = filepath.Ext(filePath)
		format = strings.Replace(format, ".", "", 1)
	}
	return parsers.Get(format)
}
