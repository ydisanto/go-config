module gitlab.com/ydisanto/go-config

go 1.17

require (
	github.com/corbym/gocrest v1.0.5
	github.com/mitchellh/mapstructure v1.4.3
	gopkg.in/yaml.v2 v2.4.0
)
