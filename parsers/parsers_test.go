package parsers

import (
	"errors"
	"github.com/corbym/gocrest/is"
	"github.com/corbym/gocrest/then"
	"testing"
)

func TestBuiltInParsers(t *testing.T) {
	then.AssertThat(t, Get("json"), is.Not(is.Nil()))
	then.AssertThat(t, Get("yml"), is.Not(is.Nil()))
}

func TestRegisteringCustomParser(t *testing.T) {
	// Arrange
	var customParser ConfigurationFileParser = func(string) (map[string]interface{}, error) {
		return map[string]interface{}{
			"foo": "bar",
		}, nil
	}

	// Act
	parser := Get("cust")
	m1, err1 := parser("dummy")

	// Assert
	then.AssertThat(t, m1, is.Nil())
	then.AssertThat(t, err1, is.EqualTo(errors.New("unsupported format: cust")))

	// Act
	Register("cust", customParser)
	parser = Get("cust")
	m2, err2 := parser("dummy")

	// Assert
	then.AssertThat(t, err2, is.Nil())
	then.AssertThat(t, len(m2), is.EqualTo(1))
	then.AssertThat(t, m2["foo"], is.EqualTo("bar"))

}
