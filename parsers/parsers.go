package parsers

import (
	"encoding/json"
	"errors"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"strings"
)

type ConfigurationFileParser func(string) (map[string]interface{}, error)

var customParsers = map[string]ConfigurationFileParser{}

func Get(format string) ConfigurationFileParser {
	format = strings.ToLower(format)
	parser := customParsers[format]
	if parser == nil {
		parser = builtInParser(format)
	}
	return parser
}

func builtInParser(format string) ConfigurationFileParser {
	var parser ConfigurationFileParser
	switch format {
	case "json":
		parser = JsonParser
		break
	case "yaml", "yml":
		parser = YamlParser
		break
	default:
		parser = UnsupportedParser(format)
		break
	}
	return parser
}

func Register(format string, parser ConfigurationFileParser) {
	customParsers[strings.ToLower(format)] = parser
}

func UnsupportedParser(format string) func(filePath string) (map[string]interface{}, error) {
	return func(filePath string) (map[string]interface{}, error) {
		return nil, errors.New("unsupported format: " + format)
	}
}

func JsonParser(filePath string) (map[string]interface{}, error) {
	return Parser(filePath, json.Unmarshal)
}

func YamlParser(filePath string) (map[string]interface{}, error) {
	return Parser(filePath, yaml.Unmarshal)
}

func Parser(filePath string, unmarshall func(data []byte, v interface{}) error) (map[string]interface{}, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer func(file *os.File) {
		_ = file.Close()
	}(file)

	bytes, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	var result map[string]interface{}
	err = unmarshall(bytes, &result)
	return result, err
}
